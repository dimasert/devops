# Docker-Compose
Docker-Compose es una herramienta de docker que permite levantar más de un contenedor de docker en un archivo de configuración, en este ejercicio, vamos a levantar el archivo y vamos a explicar el contenido del código

para probar el código, en este directorio escribir 

    docker-compose up

obviamente con docker corriendo en el pc

luego ir a http://localhost:5000 para ver el resultado

## Contenido del Docker-compose
Revisemosel código dentro de docker-compose.yml y expliquemos línea por línea

    version: '3.3'
    services:
    web:
        build: .
        ports:
        - "5000:5000"
    redis:
        image: "redis:alpine"

La arquitectura original se basa en servicios, que corresponden a una configuración de un container

La primera línea establece la versión de docker-compose, si haz bajado docker ultimamente puedes ignorar esta línea
en las siguientes líneas, podemos ver que hay dos servicios: web y redis y tienen diferentes estructura.

    `web:
        build: .
        ports:
        - "5000:5000"`

Para el contenedor web, el archivo está solicitando realizar un proceso de build (docker build) en el directorio . (el mismo donde se ejecuta docker-compose).
Esto en la práctica, implica llamar a un docker build de un Dockerfile en el mismo directorio. La diferencia está que este servicio quiere exponer un puerto interno en nuestro computador, utilizando una reserva del puerto 5000 que emula el puerto 5000 que se encuentra en la imagen.

El segundo servicio, es una llamada a un container de redis (aplicación de memorystore) que se va levantar en el mismo ambiente.

Estas dos aplicaciones son necesarias para nuestra applicación, ya que nuestro código en python necesita de un cache de memoria para ejecutarse

    cache = redis.Redis(host='redis', port=6379)

Como se puede ver, utilizar un dockercompose, puede ser tremendamente útil para construir aplicaciones que necesitan de estas herramientas intermedias, como lo son redis, o bases de datos con poca información, o quizas un servicio menor que sea dependencia de nuestra aplicación.