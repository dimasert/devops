# Taller 2

Vamos a utilizar los comandos más conocidos de Docker

## Docker Login
A loggearse con nuestro docker hub

## Docker Run
Vamos a intentar correr un contenedor de prueba

    docker run docker/whalesay cowsay Aprendo Docker

Ahora utilizaremos la opción detached -d y la opción interactive -i para habilitar un sistema operativo en el background

    docker run -di alpine /bin/ash

## Docker ps
Docker ps se utiliza para ver los contenedores que se encuentran corriendo

    docker ps

## Docker Stop
Con esto paramos un contenedor

    docker stop [NOMBRE_CONTENEDOR]

## Docker ls
Con esto podemos listar imágenes que tenemos en nuestro computador

    docker image ls

Docker ls -a nos ayuda a listar todos los contenedores que tenemos (hasta los que se encuentran detenidos )

## Docker rm
Sirve para borrar imágenes

    docker image rm docker/whalesay --force

## Docker Pull
Sirve para bajar una imagen sin correrla

    docker pull golang

## Docker Tag
Podemos renonbrar una imagen para su distribución

    docker tag golang [mi-usuario]/mi-golang

## Docker push
Ahora pudemos subir nuestra imagen a nuestro docker hub (Opcional)

    docker push [mi-usuario]/mi-golang

## Docker exec
Cuando queremos ejecutar un comando de nuestra imagen docker, en este caso intentaremos utilizar un emulador de terminal -t en una imagen
Primero creamos y partimos con una imagen con un nombre que reconzcamos

    docker run -di --name alpine alpine  /bin/ash

luego intentemos utilizar la shell de este contenedor

    docker exec -it alpine /bin/ash

Salgamos con 'exit' o apretando CONTROL+C
Finalmente paremos nuestro alpine

    docker stop alpine
