## Prueba de Run y exec

En un Dockerfile, existen dos formas de "fijar" un comando al ejecutarse un container de Dockerfile, utilizando CMD o ENDPOINT

A primera vista, estás dos instrucciones son lo mismo, que al escribir una instrucción de comandos, se ve reflejado al utilizar docker run o docker exec

La diferencia más importante es que CMD es un comando reemplazable por un parámetro de Docker.

Revisemos el contenido del Dockerfile:

    FROM ubuntu 

    RUN apt-get update && apt-get -y install python3 python3-pip
    RUN pip3 install flask flask-mysql
    COPY hello.py /opt/hola.py
    RUN chmod 777 /opt/hola.py 

    ENTRYPOINT ["python3", "/opt/hola.py"] 
    CMD ["alumno"]

La aplicación tiene un ENTRYPOINT que es el uso de python3 y un código de python, y lo que va en CMD es lo que docker run ejecuta por defecto, pero se puede cambiar.

Primero, ir al directorio para correr el Dockerfile

    cd devops-fundamentals/Taller-2/docker-exec-example

Luego crear una imagen con nombre en el directorio

    docker build -t hola .

Ahora corramos esta imagen
    docker run -i hola

Con esto corre el CMD por defecto, pero lo podemos cambiar

    docker run -i hola [TU_NOMBRE]

con esto, puedes ver otro resultado.

Así se puede usar ENTRYPOINT y CMD
