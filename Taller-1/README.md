# Taller 1

Se usará una aplicación simple de Go para instalarse en distintas estructuras


# Corriendo en APP-ENGINE (PAAS)
Bajar y Descargar [Git](https://git-scm.com) y [Go](https://golang.org/doc/install)

Primero clonamos este repositorio

    git clone https://gitlab.com/cloudlamb/devops-fundamentals.git


Ir al directorio de nuestra applicación de app-engine

    cd devops-fundamentals/Taller-1/helloworld-appengine


Luego actualizamos nuestro gcloud 

    gcloud components update


Luego creamos un proyecto (Puede ser desde la consola de GCP)

    gcloud projects create [NOMBRE_DEL_PROYECTO] --set-as-default


Habilitamos Cloud Build a este proyecto
    > Ir a https://console.developers.google.com/apis/api/cloudbuild.googleapis.com/overview?project=[NOMBRE_DEL_PROYECTO]


Creamos una applicación de app-engine

    gcloud app create --project=[NOMBRE_DEL_PROYECTO]


Descargamos la extensión de Go para gcloud

    gcloud components install app-engine-go


Deployamos nuestra aplicación

    gcloud app deploy


Revisamos nuestra app

    gcloud app browse

**NO OLVIDES DE BORRAR EL PROYECTO CUANDO TERMINES**
[Revisa tus recursos aquí](https://console.cloud.google.com/iam-admin/projects?_ga=2.27443327.238982745.1603993126-2068198787.1603993126)


# Corriendo en VM con Docker (IASS)

Primero ceamos un proyecto (Puede ser desde la consola de GCP)

    gcloud projects create [NOMBRE_DEL_PROYECTO] --set-as-default

- Habilitamos Compute Engine a este proyecto
> Ir a https://console.developers.google.com/apis/api/compute.googleapis.com/overview?project=NOMBRE_DEL_PROYECTO]

Creamos una regla de firewall para habilitar conectividad para el puerto 8080 bajo un network-tag

    gcloud compute firewall-rules create allow-8080 --allow=tcp:8080 --target-tags=allow-8080

Crear una VM con la imagen subida anteriormente, y el network tag para habilitar el puerto

    gcloud compute instances create-with-container [NOMBRE_DE_LA_VM]--container-image pabloram/go-app-taller1 --tags=allow-8080  --zone=us-east1-b

Ir a la IP publica que entrega y agregar el puerto 8080

**NO OLVIDES DE BORRAR EL PROYECTO CUANDO TERMINES**
[Revisa tus recursos aquí](https://console.cloud.google.com/iam-admin/projects?_ga=2.27443327.238982745.1603993126-2068198787.1603993126)

