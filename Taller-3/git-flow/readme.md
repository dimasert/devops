En esta parte vamos a utilizar nuestro cli, para crear un repositorio local interno, vamos a un terminal, en un directorio no protegido, y hagamos esto 

    mkdir nuestro-primer-repo

Luego, entramos a nuestro directorio

    cd nuestro-primer-repo

Transformemos nuestro directorio en un repositorio de git con:

    git init

Ahora, creamos un archivo readme.md vacío

    echo.> readme.md

Vamos por ese primer commit

    git add readme.md

Ahora, vamos a realizar un commit

    git commit -m "mi primer commit"

Ahora vamos a empujar nuestro código, para eso tenemos que entregar información sobre nuestro, repo-host, sobre nuestro proyecto/usuario/y el nombre de nuestro repo

    git push --set-upstream https://gitlab.com/cloudlamb/[nombre-unico-repo] master

Coloca tus credenciales y ya estamos subiendo el repo

Dentro vamos a modificar nuestro readme, cuando tengamos los cambios, volvamos a nuestro terminal y hacemos un pull
    git pull 

Y listo, ahora con pull y push, tenemos nuestro nuevo repositorio complemente respaldado en git
